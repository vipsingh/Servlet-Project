package shop;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Connection con;
	private PreparedStatement ps=null;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("text/html");  
		PrintWriter out=response.getWriter();  
      
		HttpSession session=request.getSession(false);
		if(session !=null)
		{
			request.getRequestDispatcher("link.html").include(request, response);
			
			try {
				ServletContext ctxt=getServletContext();
				String driverClassName=ctxt.getInitParameter("driverClassName");
				Class.forName(driverClassName);
				String url=ctxt.getInitParameter("url");
				
				String dbuser=getInitParameter("dbuser");
				String dbpass=getInitParameter("dbpass");
				String sqlst=getInitParameter("sqlstatement");
				
				con=DriverManager.getConnection(url, dbuser, dbpass);
				ps=con.prepareStatement(sqlst);
				
				out.print("<br><br><caption><center><b>E-Books:</b></center></caption><br>");
				out.print("<table width=50% border=1 align=center>");
				
				ResultSet rs = ps.executeQuery();
				
				ResultSetMetaData rsmd = rs.getMetaData();
				int total = rsmd.getColumnCount();
				out.print("<tr>");
				for(int i=1;i<=total;i++)
				{
					out.print("<th>"+rsmd.getColumnName(i)+"</th>");
				}
				out.print("<th>Buy</th>");
				out.print("</tr>");
				while(rs.next())
				{
					out.print("<tr><td align='center'>"+rs.getInt(1)+"</td><td align='center'>"+rs.getString(2)+"</td><td align='center'>"
							+rs.getString(3)+"</td><td align='center'>"+rs.getInt(4)+"</td>" +
									"<td align='center'><input type='button' value='Add to Cart'></td>");
				}
				out.print("</table>");
			}
			catch(Exception e){
				e.printStackTrace();
				throw new ServletException("Initialization failed, Unable to get DB connection");
				}
			}
		else
		{
			request.getRequestDispatcher("link.html").include(request, response);  
			out.print("You never logged IN !");    
		}
		out.close();  
		
	}	
}	