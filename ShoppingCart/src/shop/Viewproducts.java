package shop;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Viewproducts
 */
public class Viewproducts extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Viewproducts() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		
		RequestDispatcher rd=request.getRequestDispatcher("/Viewproducts.html");
		rd.include(request, response);
		HttpSession session=request.getSession(); 
		String name1=(String)session.getAttribute("name");

		String view=request.getParameter("view");
		
		String name=request.getParameter("product_name");
		String quant=request.getParameter("product_quant");
		String add=request.getParameter("add");
		
		if(add!=null){
			Cookie ck=new Cookie(name,quant);
			response.addCookie(ck);
			response.sendRedirect("Cart");
		}
		
		
		if(view!=null){
			
		
		try {
			
			ServletContext ctxt=getServletContext();
			//Getting the Driver class name from context parameter
			String driverClassName=ctxt.getInitParameter("driverClassName");
			Class.forName(driverClassName);
			//Getting the JDBC URL from context parameter
			String url=ctxt.getInitParameter("url");
			
			//Getting the DB Username, password & sqlstatement from servlet init parameter
			String dbuser=ctxt.getInitParameter("dbuser");
			String dbpass=ctxt.getInitParameter("dbpass");
			
			Connection con=DriverManager.getConnection(url,dbuser,dbpass);
	
		//out.println(con);
		
		
		String query="select product_name,product_desc from products";
		
		PreparedStatement ps=con.prepareStatement(query);
		
		ResultSet rs=ps.executeQuery();
		out.println("<br><br>");
		out.println("<html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}th, td {padding: 5px;text-align: left;}tr:nth-child(even) {background-color: #dddddd;}</style>");
		
		out.println("<body><table align='center'><caption><b>Products Available</b></caption>");
		out.println("<tr><th>Product Name</th><th>Product Description</th></tr>");
		
		
		//out.println("<p><select>");
		
		while(rs.next())
        {
			
			String s1=rs.getString(1);
			String s2=rs.getString(2);
			
			/*
			out.println("<option>"+s1+"</option>");
			out.println("<form action='Cart' method='post'>");
			*/
			out.println("<tr><td>"+s1+"</td><td>"+s2+"</td></tr>");
			
			//out.println("");
			//out.println("</form>");
    }//while
		
		//out.println("</select></p>");
		
		out.println("</table></body></head></html>");
	
	}
		//try
		catch (SQLException e) {
		
		//response.sendRedirect("error.html");
		out.println(e);
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		
		out.println(e);
		e.printStackTrace();
	}
		}

		doGet(request, response);
		
	}//doPost

}//main
