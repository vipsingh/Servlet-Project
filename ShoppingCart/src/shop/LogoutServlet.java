package shop;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
          
        
        HttpSession session=request.getSession(false);
        if(session !=null)
        {
        session.invalidate();  
        request.getRequestDispatcher("link.html").include(request, response);  
        out.print("<center>You are successfully logged out!</center>");
        }
        else
        {
        request.getRequestDispatcher("link.html").include(request, response);  
        out.print("<center>You never logged IN !</center>");    
        }
        
        out.close();  
	}
}
