package shop;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class add_product
 */
public class add_product extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public add_product() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		HttpSession session=request.getSession(); 
		String name=(String)session.getAttribute("admin");
		
		String product=request.getParameter("product");
		String desc=request.getParameter("desc");
		
		try {
			ServletContext ctxt=getServletContext();
			//Getting the Driver class name from context parameter
			String driverClassName=ctxt.getInitParameter("driverClassName");
			Class.forName(driverClassName);
			//Getting the JDBC URL from context parameter
			String url=ctxt.getInitParameter("url");
			
			//Getting the DB Username, password & sqlstatement from servlet init parameter
			String dbuser=ctxt.getInitParameter("dbuser");
			String dbpass=ctxt.getInitParameter("dbpass");
			
			Connection con=DriverManager.getConnection(url, dbuser, dbpass);
			
		//out.println(con);			//Testing
		
		String query="insert into products values(?,?)";
		
		PreparedStatement ps=con.prepareStatement(query);
		
		ps.setString(1, product);
		ps.setString(2, desc);
		
		int count=ps.executeUpdate();

		if(count>0){
			
			RequestDispatcher rd1=request.getRequestDispatcher("addproduct.html");  
		    rd1.include(request, response);  
			//Testing Purpose
			out.println("<center><b>Record inserted Successfully count:"+count+" </b></center>");
			//out.println(""+con);			//Testing
			
		}
		else{
			//response.sendRedirect("error.html");
			out.println("Products not Inserted");
		}
		con.close();
		
		
	} catch (SQLException e) {
		
		//response.sendRedirect("error.html");
		RequestDispatcher rd1=request.getRequestDispatcher("/addproduct.html");
		rd1.include(request, response);;
		out.println("<center><b><i><u>"+e+"</u></i></b></center>");
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		
		out.println(e);
		e.printStackTrace();
	}
		
	
		doGet(request, response);
	}

}
