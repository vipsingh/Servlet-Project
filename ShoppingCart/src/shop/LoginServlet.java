package shop;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Connection con;
	private PreparedStatement ps=null;

	public void init() throws ServletException {
		try {
			ServletContext ctxt=getServletContext();
			String driverClassName=ctxt.getInitParameter("driverClassName");
			Class.forName(driverClassName);
			String url=ctxt.getInitParameter("url");
			
			String dbuser=getInitParameter("dbuser");
			String dbpass=getInitParameter("dbpass");
			String sqlst=getInitParameter("sqlstatement");
			
			con=DriverManager.getConnection(url, dbuser, dbpass);
			ps=con.prepareStatement(sqlst);
		}
		catch(Exception e){
			e.printStackTrace();
			throw new ServletException("Initialization failed, Unable to get DB connection");
		}
	}

	public void service (ServletRequest request, ServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		try {

			String name=request.getParameter("user");
			String password= request.getParameter("pwd");

			if (name==null||name.equals("")||password==null||password.equals("")) 
			{ 
		        out.print("Sorry, username or password can not be empty!");  	
			}
			ps.setString(1,name);
			ps.setString(2,password);
			ResultSet rs=ps.executeQuery();

			if (rs.next()){
				request.getRequestDispatcher("link.html").include(request, response);     
		        out.print("<br><br><center>Welcome, "+name+"</center><br><br><br>");
		        out.print("<form action='HomeServlet' align='center'>");
		        out.print("<input type='submit' value='Click Here to See List of Books Details'/>");
		        out.print("</form>");
		        
		        HttpSession session=((HttpServletRequest) request).getSession();  
		        session.setAttribute("name",name);
				}
			else{
				request.getRequestDispatcher("link.html").include(request, response);     
		        out.print("Sorry You are not register with us..");  
		        }
		}
		catch(Exception e){
			out.println("<html><body><center>");
			out.println("<h2>Unable to the process the request try after some time</h2>");
			out.println("</center></body></html>");
		}
	}
	
	
	/*protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
   		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        
          
        String name=request.getParameter("user");  
        String password=request.getParameter("pwd");  
          
        if(name!=null && password.equals("admin123")){
        request.getRequestDispatcher("link.html").include(request, response);     
        out.print("Welcome, "+name+"<br>");  
        
        HttpSession session=request.getSession();  
        session.setAttribute("name",name);  
        }  
        else{  
        request.getRequestDispatcher("index.html").include(request, response);  
        out.print("Sorry, username or password error!");  
        }  
        out.close();  
    }*/
	
}
