package shop;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Cart
 */
public class Cart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		
		HttpSession session=request.getSession(); 
		String name1=(String)session.getAttribute("name");
		
		RequestDispatcher rd=request.getRequestDispatcher("/Cart.html");
		rd.include(request, response);
		
		out.println("<html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}th, td {padding: 5px;text-align: left;}tr:nth-child(even) {background-color: #dddddd;}</style>");
		out.println("<body><table align='center'><caption><b>Items Added to Cart</b></caption>");
		out.println("<tr><th>Product Name</th><th>Product Quantity</th></tr>");
		String buy=request.getParameter("buy");
		
		Cookie ck[]=request.getCookies();
		for(int i=0;i<ck.length;i++){
			out.println("<tr><td>"+ck[i].getName()+"</td><td>"+ck[i].getValue()+"</td></tr>");
		}	
		out.println("</table></body></head></html>");
		
		String name=request.getParameter("product_name");
		String deletebtn=request.getParameter("delete");
		
		//Deleting a Cookie		
		
		if(deletebtn!=null){
			
			Cookie[] cookies = request.getCookies();
			if(ck!=null){
				int i = 0;
					if(name.equals(cookies[i].getName())){
					cookies[i].setMaxAge(0);
					response.addCookie(cookies[i]);
					response.sendRedirect("Cart");
					}//if Comparing
					else{
						out.println("<b>Wrong Product Name or<br>");
						out.println("Product have Not Been Added to cart</b>");
					}
						
			}//if Checking
				
			}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
