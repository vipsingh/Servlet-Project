package shop;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Payment
 */
public class Payment extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private int count;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Payment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		try {
			ServletContext ctxt=getServletContext();
			//Getting the Driver class name from context parameter
			String driverClassName=ctxt.getInitParameter("driverClassName");
			Class.forName(driverClassName);
			//Getting the JDBC URL from context parameter
			String url=ctxt.getInitParameter("url");
			
			//Getting the DB Username, password & sqlstatement from servlet init parameter
			String dbuser=ctxt.getInitParameter("dbuser");
			String dbpass=ctxt.getInitParameter("dbpass");
			
			Connection con=DriverManager.getConnection(url, dbuser, dbpass);
			
		//out.println(con);			//Testing
			
		String query="insert into orders values(?,?)";
		
		PreparedStatement ps=con.prepareStatement(query);
		
		Cookie ck[]=request.getCookies();
		for(int i=0;i<ck.length;i++){
			
		ps.setString(1,ck[i].getName());
		ps.setString(2, ck[i].getValue());
		
		
		int count=ps.executeUpdate();
		}
		
		
		RequestDispatcher rd=request.getRequestDispatcher("Logout");
		
		rd.include(request, response);
		out.println("<br><center><b><u>Thank you for Shopping<br>Please Visit Again!</u><b></center>");
		
		con.close();
		
		
	} catch (SQLException e) {
		
		//response.sendRedirect("error.html");
		RequestDispatcher rd1=request.getRequestDispatcher("/Viewproducts.html");
		rd1.include(request, response);;
		out.println("<center><b><i><u>"+e+"</u></i></b></center>");
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		
		out.println(e);
		e.printStackTrace();
	}
		doGet(request, response);
	}

}
