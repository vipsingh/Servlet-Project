package shop;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;

public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Connection con;
	private PreparedStatement ps=null;

	public void init() throws ServletException {
		try {
			ServletContext ctxt=getServletContext();
			String driverClassName=ctxt.getInitParameter("driverClassName");
			Class.forName(driverClassName);
			String url=ctxt.getInitParameter("url");
			
			String dbuser=getInitParameter("dbuser");
			String dbpass=getInitParameter("dbpass");
			String sqlst=getInitParameter("sqlstatement");
			
			con=DriverManager.getConnection(url, dbuser, dbpass);
			ps=con.prepareStatement(sqlst);
		}
		catch(Exception e){
			e.printStackTrace();
			throw new ServletException("Initialization failed, Unable to get DB connection");
		}
	}

	public void service (ServletRequest req, ServletResponse res) throws ServletException, IOException {

		System.out.println("In service");

		res.setContentType("text/html");
		PrintWriter out=res.getWriter();
		try {

			String uname=req.getParameter("uname");
			String pass= req.getParameter("pass");
			String repass= req.getParameter("repass");

			if (uname==null||uname.equals("")
				||pass==null||pass.equals("")
				||!pass.equals(repass)) {
				
				req.getRequestDispatcher("link.html").include(req, res);  
		        out.println("<center>Given details are not valid to register</center>");
				out.println("<center>Please try again later</center>");
				return;
			}

			String addr=req.getParameter("addr");
			String phone=req.getParameter("phone");
			String email=req.getParameter("email");
			
			ps.setString(1,uname);
			ps.setString(2,pass);
			ps.setString(3,addr);
			ps.setString(4,phone);
			ps.setString(5,email);

			int count=ps.executeUpdate();

			if (count==1)
			{
				 out.println("<html><script>alert('Registered SuccessFully.Please login using username & password');" +
				 		"window.location='index.html';</script></html>");
				 //req.getRequestDispatcher("index.html").forward(req, res); 
				
			}
			else{
				out.println("<center>Given details are incorrect</center><br/>");
				out.println("<center>Please try again later</center>");
			}
		}
		catch(Exception e){
			out.println("<html><body><center>");
			out.println("<h2>Unable to the process the request try after some time</h2>");
			out.println("</center></body></html>");
		}
	}
}